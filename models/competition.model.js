const mongoose = require("mongoose");

const CompetitionSchema = new mongoose.Schema(
  {
    creatorId: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      trim: true,
      maxlength: 500,
    },
    startDate: {
      type: String,
    },
    sport: {
      type: String,
    },
    participants: {
      type: [
        {
          participantId: String,
          rank: Number,
        },
      ],
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("competition", CompetitionSchema);
