const mongoose = require('mongoose');
const { isEmail } = require('validator');
const bcrypt = require ('bcrypt');

const userSchema = new mongoose.Schema(
    {
        pseudo: {
            type: String,
            required: true,
            minLength: 3,
            maxLength: 55,
            unique: true,
            trim: true
        },
        email: {
            type: String,
            required: true,
            minLength: 3,
            validate:[isEmail],
            lowercase: true,
            unique: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 1024
        },
        picture: {
            type: String,
            default: "/uploads/profil/default-user.png"
        },
        bio: {
            type: String,
            max: 1024,
            default: ""
        },
        following: {    // personne que l'utilisateur follow
            type: [String]
        },
        followers: {    // personne qui follow l'utilisateur
            type: [String]
        },
        likes: {        // liste des posts Liké par l'utilisateur  TODO -> peut etre changer et mettre plutot sur le post, la liste des personnes qui Like
            type: [String]
        }
    },
    {
        timestamps: true,
    }
);

// fonction appellé avant sauvegarde sur la BD
userSchema.pre("save", async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);     // pour crypter le MDP
    next();
});

userSchema.statics.login = async function(email, password) {
    const user = await this.findOne({ email });
    if (user) {
        const auth = await bcrypt.compare(password, user.password);
        if (auth) {
            return user;
        }
        throw Error('Mot de passe incorrecte');
    }
    throw Error('Email incorrecte')
}


const UserModel = mongoose.model('user',userSchema);

module.exports = UserModel;