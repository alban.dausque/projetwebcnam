const mongoose = require('mongoose');

mongoose
    .connect("mongodb+srv://" + process.env.DB_USER_PASS + "@projetwebcnam.4gv8bsb.mongodb.net/ProjetWebCnam")
    .then(() => console.log('Connecté à MongoDB'))
    .catch((err) => console.log('Erreur à la connexion de MongoDB :', err));