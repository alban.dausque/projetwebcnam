const UserModel = require("../models/user.model");
const ObjectId = require("mongoose").Types.ObjectId;
const fs = require("fs");
const xss = require("xss");

module.exports.getAllUsers = async (req, res) => {
  const users = await UserModel.find().select("-password"); // '-password' pour ne pas envoyer le MDP
  res.status(200).json(users);
};

module.exports.userInfo = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectId.isValid(id))
    return res.status(400).json({ message: "ID inconnu : " + id });

  try {
    const info = await UserModel.findById(id).select("-password");
    if (!info) {
      return res.status(400).send("ID inconnu : " + id);
    }
    res.send(info);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.updateUser = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectId.isValid(id))
    return res.status(400).json({ message: "ID inconnu : " + id });

  try {
    const updatedUser = await UserModel.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          bio: xss(req.body.bio),
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true }
    ).select("-password");

    if (!updatedUser) {
      return res.status(404).send({ message: "Utilisateur non trouvé." });
    }

    return res.send(updatedUser);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.deleteUser = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectId.isValid(id))
    return res.status(400).json({ message: "ID inconnu : " + id });

  try {
    const user = await UserModel.findById(id);
    if (!user) {
      return res
        .status(400)
        .json({ message: "Utilisateur non trouvé : " + id });
    }

    // suppression de l'image du profil
    if (user.picture !== "/uploads/profil/default-user.png") {
      fs.unlink("./client/public" + user.picture.slice(1), (err) => {
        if (err) {
          console.error("Erreur lors de la suppression du fichier:", err);
          return;
        }
      });
    }

    const result = await UserModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      return res.status(400).json({ message: "ID inconnu : " + id });
    }

    return res.status(200).json({ message: "Suppression réussie" });
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.follow = async (req, res) => {
  const id = xss(req.params.id);
  const idToFollow = xss(req.body.idToFollow);

  if (!ObjectId.isValid(id))
    return res.status(400).json({ message: "ID inconnu : " + id });
  if (!ObjectId.isValid(idToFollow))
    return res
      .status(400)
      .json({ message: "ID inconnu : " + idToFollow });

  try {
    // ajout à la liste de following
    const userFollowing = await UserModel.findByIdAndUpdate(
      id,
      { $addToSet: { following: idToFollow } },
      { new: true, upsert: true }
    );

    // ajout à la liste de follower
    const userBeingFollowed = await UserModel.findByIdAndUpdate(
      idToFollow,
      { $addToSet: { followers: id } },
      { new: true, upsert: true }
    );

    if (!userBeingFollowed || !userFollowing) {
      return res.status(404).json({ message: "User not found" });
    }

    return res.status(201).json(userFollowing);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.unfollow = async (req, res) => {
  const id = xss(req.params.id);
  const idToUnfollow = xss(req.body.idToUnfollow);

  if (!ObjectId.isValid(id))
    return res.status(400).json({ message: "ID inconnu : " + id });
  if (!ObjectId.isValid(idToUnfollow))
    return res
      .status(400)
      .json({ message: "ID inconnu : " + idToUnfollow });

  try {
    // ajout à la liste de following
    const userFollowing = await UserModel.findByIdAndUpdate(
      id,
      { $pull: { following: idToUnfollow } },
      { new: true, upsert: true }
    );

    // ajout à la liste de follower
    const userBeingFollowed = await UserModel.findByIdAndUpdate(
      idToUnfollow,
      { $pull: { followers: id } },
      { new: true, upsert: true }
    );

    if (!userBeingFollowed || !userFollowing) {
      return res.status(404).json({ message: "User not found" });
    }

    return res.status(201).json(userFollowing);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};
