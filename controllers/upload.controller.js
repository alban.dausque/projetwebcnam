const UserModel = require("../models/user.model");
const fs = require("fs");
const path = require("path");
const { uploadErrors } = require("../utils/errors.utils");
const xss = require("xss");

module.exports.uploadProfil = async (req, res) => {
  try {
    if (
      req.file.mimetype != "image/jpg" &&
      req.file.mimetype != "image/png" &&
      req.file.mimetype != "image/jpeg"
    ) {
      fs.unlink(req.file.path, (err) => {
        if (err) {
          console.error("Erreur lors de la suppression du fichier:", err);
          return;
        }
      });
      throw Error("invalid file");
    }

    if (req.file.size > 10000000) {
      fs.unlink(req.file.path, (err) => {
        if (err) {
          console.error("Erreur lors de la suppression du fichier:", err);
          return;
        }
      });
      throw Error("max size");
    }
  } catch (err) {
    const errors = uploadErrors(err);
    return res.status(201).json({ errors });
  }

  const fileName = xss(req.body.userId) + ".jpg"; // utilise l'Id du profil comme nom de fichier

  fs.rename(
    req.file.path,
    path.join(path.dirname(req.file.path), fileName),
    (err) => {
      if (err) {
        console.error("Erreur lors du renommage du fichier:", err);
        return;
      }
    }
  );

  try {
    const updatedUser = await UserModel.findByIdAndUpdate(
      xss(req.body.userId),
      { $set: { picture: "/uploads/profil/" + fileName } },
      { new: true, upsert: true, setDefaultsOnInsert: true }
    );

    if (updatedUser) {
      res.send(updatedUser);
    } else {
      res.status(404).send({ message: "Utilisateur non trouvé" });
    }
  } catch (err) {
    console.error("Erreur lors de la mise à jour de l'utilisateur :", err);
    res.status(500).send({ message: "Erreur serveur", error: err });
  }
};
