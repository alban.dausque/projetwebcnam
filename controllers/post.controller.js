const postModel = require("../models/post.model");
const PostModel = require("../models/post.model");
const UserModel = require("../models/user.model");
const { uploadErrors } = require("../utils/errors.utils");
const ObjectID = require("mongoose").Types.ObjectId;
const fs = require("fs");
const path = require("path");
const xss = require("xss");

module.exports.readPost = async (req, res) => {
  try {
    const docs = await PostModel.find().sort({ createdAt: -1 });
    res.send(docs);
  } catch (err) {
    console.log("Erreur pour récupérer les données : " + err);
    res.status(500).send("Erreur pour récupérer les données");
  }
};

module.exports.createPost = async (req, res) => {
  let fileName;

  if (req.file !== undefined) {
    try {
      if (
        req.file.mimetype != "image/jpg" &&
        req.file.mimetype != "image/png" &&
        req.file.mimetype != "image/jpeg"
      ) {
        fs.unlink(req.file.path, (err) => {
          if (err) {
            console.error("Erreur lors de la suppression du fichier:", err);
            return;
          }
        });
        throw Error("invalid file");
      }

      if (req.file.size > 10000000) {
        fs.unlink(req.file.path, (err) => {
          if (err) {
            console.error("Erreur lors de la suppression du fichier:", err);
            return;
          }
        });
        throw Error("max size");
      }
    } catch (err) {
      const errors = uploadErrors(err);
      return res.status(201).json({ errors });
    }

    fileName = xss(req.body.posterId) + Date.now() + ".jpg";

    fs.rename(
      req.file.path,
      path.join(path.dirname(req.file.path), fileName),
      (err) => {
        if (err) {
          console.error("Erreur lors du renommage du fichier:", err);
          return;
        }
      }
    );
  }

  const newPost = new postModel({
    posterId: xss(req.body.posterId),
    message: xss(req.body.message),
    picture: req.file !== undefined ? "/uploads/posts/" + fileName : "",
    likers: [],
    comments: [],
  });

  try {
    const post = await newPost.save();
    return res.status(201).json(post);
  } catch (err) {
    return res.status(400).send(err);
  }
};

module.exports.updatePost = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID inconnu : " + id);
  }

  const updatedRecord = {
    message: req.body.message,
  };

  try {
    const docs = await PostModel.findByIdAndUpdate(
      id,
      { $set: updatedRecord },
      { new: true }
    );
    if (!docs) {
      res.status(404).send("Aucun post trouvé avec cet ID");
    } else {
      res.send(docs);
    }
  } catch (err) {
    console.log("Erreur lors de la mise à jour : " + err);
    res.status(500).send("Erreur lors de la mise à jour");
  }
};

module.exports.deletePost = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID inconnu : " + id);
  }

  try {
    const post = await PostModel.findById(id);
    if (!post) {
      return res
        .status(400)
        .json({ message: "Post non trouvé : " + id });
    }

    // suppression de l'image du post
    if (post.picture !== "" && post.picture !== "/uploads/posts/undefined") {
      fs.unlink("./client/public" + post.picture.slice(1), (err) => {
        if (err) {
          console.error("Erreur lors de la suppression du fichier:", err);
          return;
        }
      });
    }

    const deletedPost = await PostModel.findByIdAndDelete(id);
    if (!deletedPost) {
      res.status(404).send("Aucun post trouvé avec cet ID");
    } else {
      res.send(deletedPost);
    }
  } catch (err) {
    console.log("Erreur lors de la suppression du post : " + err);
    res.status(500).send("Erreur lors de la suppression du post");
  }
};

module.exports.likePost = async (req, res) => {
  const id = xss(req.params.id);
  const bodyId = xss(req.body.id);

  if (!ObjectID.isValid(id) || !ObjectID.isValid(bodyId)) {
    return res.status(400).send("ID inconnu ou invalide");
  }

  try {
    const postUpdateResult = await PostModel.findByIdAndUpdate(
      id,
      { $addToSet: { likers: bodyId } },
      { new: true }
    );

    if (!postUpdateResult) {
      return res.status(404).send("Post non trouvé");
    }

    const userUpdateResult = await UserModel.findByIdAndUpdate(
      bodyId,
      { $addToSet: { likes: id } },
      { new: true }
    );

    if (!userUpdateResult) {
      return res.status(404).send("Utilisateur non trouvé");
    }

    res.send(userUpdateResult);
  } catch (err) {
    console.error("Erreur lors de la mise à jour des likes", err);
    res.status(500).send("Erreur lors de la mise à jour des likes");
  }
};

module.exports.unlikePost = async (req, res) => {
  const id = xss(req.params.id);
  const bodyId = xss(req.body.id);

  if (!ObjectID.isValid(id) || !ObjectID.isValid(bodyId)) {
    return res.status(400).send("ID inconnu ou invalide");
  }

  try {
    const postUpdateResult = await PostModel.findByIdAndUpdate(
      id,
      { $pull: { likers: bodyId } },
      { new: true }
    );

    if (!postUpdateResult) {
      return res.status(404).send("Post non trouvé");
    }

    const userUpdateResult = await UserModel.findByIdAndUpdate(
      bodyId,
      { $pull: { likes: id } },
      { new: true }
    );

    if (!userUpdateResult) {
      return res.status(404).send("Utilisateur non trouvé");
    }

    res.send(userUpdateResult);
  } catch (err) {
    console.error("Erreur lors de la suppression du like", err);
    res.status(500).send("Erreur serveur lors de la suppression du like");
  }
};

module.exports.commentPost = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID inconnu : " + id);
  }

  // Assurez-vous que les champs requis sont présents
  if (!req.body.commenterId || !req.body.commenterPseudo || !req.body.text) {
    return res.status(400).send("Des informations sont manquantes");
  }

  try {
    const updatedPost = await PostModel.findByIdAndUpdate(
      id,
      {
        $push: {
          comments: {
            commenterId: xss(req.body.commenterId),
            commenterPseudo: xss(req.body.commenterPseudo),
            text: xss(req.body.text),
            timestamp: new Date().getTime(),
          },
        },
      },
      { new: true }
    );

    if (!updatedPost) {
      return res.status(404).send("Post non trouvé");
    }

    res.send(updatedPost);
  } catch (err) {
    console.error("Erreur lors de l'ajout d'un commentaire", err);
    res.status(500).send("Erreur lors de l'ajout d'un commentaire");
  }
};

module.exports.editCommentPost = async (req, res) => {
  const id = xss(req.params.id);

  if (
    !ObjectID.isValid(id) ||
    !ObjectID.isValid(xss(req.body.commentId))
  ) {
    return res.status(400).send("ID inconnu ou invalide");
  }

  try {
    const post = await PostModel.findById(id);
    if (!post) {
      return res.status(404).send("Post non trouvé");
    }

    const comment = post.comments.find((comment) =>
      comment._id.equals(xss(req.body.commentId))
    );
    if (!comment) {
      return res.status(404).send("Commentaire non trouvé");
    }

    comment.text = xss(req.body.text);

    await post.save();
    res.send(post);
  } catch (err) {
    console.error("Erreur lors de la mise à jour du commentaire", err);
    res
      .status(500)
      .send("Erreur serveur lors de la mise à jour du commentaire");
  }
};

module.exports.deleteCommentPost = async (req, res) => {
  const id = xss(req.params.id);

  if (
    !ObjectID.isValid(id) ||
    !ObjectID.isValid(xss(req.body.commentId))
  ) {
    return res.status(400).send("ID inconnu ou invalide");
  }

  try {
    const postUpdateResult = await PostModel.findByIdAndUpdate(
      id,
      { $pull: { comments: { _id: xss(req.body.commentId) } } },
      { new: true }
    );

    if (!postUpdateResult) {
      return res.status(404).send("Post non trouvé ou commentaire non trouvé");
    }

    res.send(postUpdateResult);
  } catch (err) {
    console.error("Erreur lors de la suppression du commentaire", err);
    res
      .status(500)
      .send("Erreur serveur lors de la suppression du commentaire");
  }
};
