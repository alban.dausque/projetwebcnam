const CompetitionModel = require("../models/competition.model");
const ObjectID = require("mongoose").Types.ObjectId;
const xss = require("xss");

module.exports.createCompetition = async (req, res) => {
  //const { creatorId, description, startDate, sport } = req.body;
  const creatorId = xss(req.body.creatorId);
  const description = xss(req.body.description);
  const startDate = xss(req.body.startDate);
  const sport = xss(req.body.sport);

  const newCompetition = new CompetitionModel({
    creatorId,
    description,
    startDate,
    sport,
  });

  try {
    const competition = await newCompetition.save();
    res.status(201).json(competition);
  } catch (err) {
    res.status(400).send(err);
  }
};

module.exports.readCompetition = async (req, res) => {
  try {
    const competitions = await CompetitionModel.find().sort({ createdAt: -1 });
    res.send(competitions);
  } catch (err) {
    res
      .status(500)
      .send("Erreur pour récupérer les données de compétition : " + err);
  }
};

module.exports.updateCompetition = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID de compétition inconnu : " + id);
  }

  const updatedData = {
    description: xss(req.body.description),
    startDate: xss(req.body.startDate),
    sport: xss(req.body.sport),
  };

  try {
    const updatedCompetition = await CompetitionModel.findByIdAndUpdate(
      id,
      { $set: updatedData },
      { new: true }
    );
    if (!updatedCompetition) {
      res.status(404).send("Aucune compétition trouvée avec cet ID");
    } else {
      res.send(updatedCompetition);
    }
  } catch (err) {
    res
      .status(500)
      .send("Erreur lors de la mise à jour de la compétition : " + err);
  }
};

module.exports.deleteCompetition = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID de compétition inconnu : " + id);
  }

  try {
    const deletedCompetition = await CompetitionModel.findByIdAndDelete(
      id
    );
    if (!deletedCompetition) {
      res.status(404).send("Aucune compétition trouvée avec cet ID");
    } else {
      res.send("Compétition supprimée avec succès");
    }
  } catch (err) {
    res
      .status(500)
      .send("Erreur lors de la suppression de la compétition : " + err);
  }
};

module.exports.addParticipant = async (req, res) => {
  const id = xss(req.params.id);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID de compétition inconnu ou invalide");
  }

  const { participantId } = req.body;
  try {
    const competition = await CompetitionModel.findByIdAndUpdate(
      id,
      { $push: { participants: { participantId } } },
      { new: true }
    );
    if (!competition) {
      return res.status(404).send("Compétition non trouvée");
    }
    res.send(competition);
  } catch (err) {
    res.status(500).send("Erreur lors de l'ajout du participant : " + err);
  }
};

module.exports.updateParticipants = async (req, res) => {
  const id = xss(req.params.id);
  const participantId = xss(req.body.participantId);
  const rank = xss(req.body.rank);

  if (
    !ObjectID.isValid(id) ||
    !ObjectID.isValid(participantId)
  ) {
    return res.status(400).send("ID inconnu ou invalide");
  }

  try {
    const competition = await CompetitionModel.findById(id);
    if (!competition) {
      return res.status(404).send("Compétition non trouvée");
    }

    const participant = competition.participants.find((p) =>
      p._id.equals(participantId)
    );
    if (!participant) {
      return res.status(404).send("Participant non trouvé");
    }

    participant.rank = rank;

    await competition.save();
    res.send(competition);
  } catch (err) {
    console.error("Erreur lors de la mise à jour des participants", err);
    res
      .status(500)
      .send("Erreur serveur lors de la mise à jour des participants");
  }
};

module.exports.deleteParticipant = async (req, res) => {
  const id = xss(req.params.id);
  const participantId = xss(req.body.participantId);
  
  if (!ObjectID.isValid(id)) {
    return res.status(400).send("ID de compétition inconnu ou invalide");
  }
  
  try {
    const competition = await CompetitionModel.findByIdAndUpdate(
      id,
      { $pull: { participants: { _id: participantId } } },
      { new: true }
    );
    if (!competition) {
      return res
        .status(404)
        .send("Compétition non trouvée ou participant non trouvé");
    }
    res.send(competition);
  } catch (err) {
    res
      .status(500)
      .send("Erreur lors de la suppression du participant : " + err);
  }
};
