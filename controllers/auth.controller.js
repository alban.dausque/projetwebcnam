const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");
const { signUpErrors, signInErrors } = require("../utils/errors.utils");
const { Resend } = require("resend");
const xss = require("xss");

module.exports.signUp = async (req, res) => {
  //const { pseudo, email, password } = req.body;
  const pseudo = xss(req.body.pseudo);
  const email = xss(req.body.email);
  const password = xss(req.body.password);

  try {
    const user = await UserModel.create({ pseudo, email, password });

    const resend = new Resend(process.env.RESEND_API_KEY);

    const { data, error } = await resend.emails.send({
      from: "onboarding@resend.dev",
      to: email,
      subject: "Bienvenue sur Sports Social Network",
      html: "<p>Votre compte sur Sports Social Network a bien été créé !</p><p>Vous pouvez maintenant vous connecter !</p>",
    });

    if (error) {
      return res.status(400).json({ error });
    }

    res.status(201).json({ user: user._id });
  } catch (err) {
    const errors = signUpErrors(err);
    res.status(200).send({ err });
  }
};

const maxAge = 3 * 24 * 60 * 60 * 1000; // validité du token (3 * ... = 3 jours)
const createToken = (id) => {
  return jwt.sign({ id }, process.env.TOKEN_SECRET, {
    expiresIn: maxAge,
  });
};

module.exports.signIn = async (req, res) => {
  //const { email, password } = req.body;
  const email = xss(req.body.email);
  const password = xss(req.body.password);

  try {
    const user = await UserModel.login(email, password);
    const token = createToken(user._id);
    res.cookie("jwt", token, {
      httpOnly: true,
      secure: true,
      maxAge: maxAge,
      sameSite: "strict",
    });
    res.status(200).json({ user: user._id });
  } catch (err) {
    const errors = signInErrors(err);
    res.status(200).send({ errors });
  }
};

module.exports.logOut = (req, res) => {
  res.cookie("jwt", "", { maxAge: 1 });
  res.redirect("/");
};
