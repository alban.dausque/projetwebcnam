/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {
      whitespace: {
        "pre-wrap": "pre-wrap",
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
