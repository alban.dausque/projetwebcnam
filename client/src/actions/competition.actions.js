import axios from "axios";

export const GET_COMPETITIONS = "GET_COMPETITIONS";
export const DELETE_COMPETITION = "DELETE_COMPETITION";
export const ADD_PARTICIPANT = "ADD_PARTICIPANT";
export const DELETE_PARTICIPANT = "DELETE_PARTICIPANT";
export const UPDATE_PARTICIPANT = "UPDATE_PARTICIPANT";

export const getCompetitions = () => {
  return (dispatch) => {
    return axios
      .get(`${process.env.REACT_APP_API_URL}api/competition/`)
      .then((res) => {
        dispatch({ type: GET_COMPETITIONS, payload: res.data });
      })
      .catch((err) => console.log(err));
  };
};

export const deleteCompetition = (competitionId) => {
    return (dispatch) => {
      return axios({
        method: "delete",
        url: `${process.env.REACT_APP_API_URL}api/competition/${competitionId}`,
      })
        .then((res) => {
          dispatch({ type: DELETE_COMPETITION, payload: { competitionId } });
        })
        .catch((err) => console.log(err));
    };
  };

export const addParticipant = (competitionId, participantId) => {
  return (dispatch) => {
    return axios({
      method: "patch",
      url: `${process.env.REACT_APP_API_URL}api/competition/participants/${competitionId}`,
      data: { participantId },
    })
      .then((res) => {
        dispatch({ type: ADD_PARTICIPANT, payload: { competitionId } });
      })
      .catch((err) => console.log(err));
  };
};

export const updateParticipant = (competitionId, participantId, rank) => {
    return (dispatch) => {
      return axios({
        method: "patch",
        url: `${process.env.REACT_APP_API_URL}api/competition/update-participant/${competitionId}`,
        data: { participantId, rank },
      })
        .then((res) => {
          dispatch({ type: UPDATE_PARTICIPANT, payload: { competitionId, participantId, rank } });
        })
        .catch((err) => console.log(err));
    };
  };

export const deleteParticipant = (competitionId, participantId) => {
  return (dispatch) => {
    return axios({
      method: "patch",
      url: `${process.env.REACT_APP_API_URL}api/competition/delete-participants/${competitionId}`,
      data: { participantId },
    })
      .then((res) => {
        dispatch({
          type: DELETE_PARTICIPANT,
          payload: { competitionId, participantId },
        });
      })
      .catch((err) => console.log(err));
  };
};
