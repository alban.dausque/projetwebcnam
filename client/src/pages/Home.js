import React, { useContext } from "react";
import { UidContext } from "../components/AppContext";
import LateralNav from "../components/LateralNav";
import NewPostForm from "../components/Post/NewPostForm";
import Thread from "../components/Thread";
import Log from "../components/Log/index";

const Home = () => {
  const uid = useContext(UidContext);

  return (
    <div className="flex flex-col items-center">
      <LateralNav />
      <div className="flex flex-col items-center">
        <div className="flex flex-col items-center w-full">
          {uid ? <NewPostForm /> : <Log signin={true} signup={false} />}
        </div>
        <Thread />
      </div>
    </div>
  );
};

export default Home;
