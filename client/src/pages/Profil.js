import React, { useContext } from "react";
import Log from "../components/Log/index";
import { UidContext } from "../components/AppContext";
import UpdateProfil from "../components/Profil/UpdateProfil";
import { useParams } from "react-router-dom";
import VisitProfil from "../components/Profil/VisitProfil";
import { useSelector } from "react-redux";
import { isEmpty } from "../components/Utils";
import Card from "../components/Post/Card";
import LateralNav from "../components/LateralNav";

const Profil = () => {
  let { pseudo } = useParams();
  const uid = useContext(UidContext);
  const usersData = useSelector((state) => state.usersReducer);
  const posts = useSelector((state) => state.postReducer);

  return (
    <div className="">
      <LateralNav />
      {pseudo ? (
        <>
          {usersData.map((user) => {
            if (user.pseudo === pseudo) {
              return (
                <div className="flex flex-col justify-center items-center">
                  <VisitProfil user_visit={user} key={user._id} />
                  <ul className="m-3 w-2/5">
                    {!isEmpty(posts[0]) &&
                      posts.map((post) => {
                        if (post.posterId === user._id)
                          return <Card post={post} key={post._id} />;
                        return null;
                      })}
                  </ul>
                </div>
              );
            }
            return null;
          })}
        </>
      ) : (
        <>
          {uid ? (
            <div className="flex flex-col justify-center items-center">
              <UpdateProfil key={uid} />
              <ul className="m-3 w-2/5">
                {!isEmpty(posts[0]) &&
                  posts.map((post) => {
                    if (post.posterId === uid)
                      return <Card post={post} key={post._id} />;
                    return null;
                  })}
              </ul>
            </div>
          ) : (
            <div className="flex justify-center items-center">
              <Log signin={false} signup={true} />
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default Profil;
