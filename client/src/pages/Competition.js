import React, { useEffect } from "react";
import LateralNav from "../components/LateralNav";
import NextCompetition from "../components/Competition/NextCompetition";
import ResultCompetition from "../components/Competition/ResultCompetition";
import CreateCompetition from "../components/Competition/CreateCompetition";
import { getCompetitions } from "../actions/competition.actions";
import { useDispatch } from "react-redux";

const Competition = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCompetitions());
  }, [dispatch]);

  return (
    <div>
      <LateralNav />
      <div className="grid grid-cols-3">
        <div className="flex justify-center text-center">
          <NextCompetition />
        </div>
        <div className="flex justify-center text-center">
          <ResultCompetition />
        </div>
        <div className="flex justify-center text-center">
          <CreateCompetition />
        </div>
      </div>
    </div>
  );
};

export default Competition;
