import React, { useState } from "react";
import { useSelector } from "react-redux";
import FollowHandler from "../components/Profil/FollowHandler";
import { NavLink } from "react-router-dom";
import LateralNav from "../components/LateralNav";

const Search = () => {
  const [userSearch, setUserSearch] = useState("");
  const userData = useSelector((state) => state.userReducer);
  const usersData = useSelector((state) => state.usersReducer);

  return (
    <div className="flex flex-col items-center">
      <LateralNav />
      <div className="cadre-log w-4/12">
        <h3 className="ml-2">Rechercher un profil</h3>
        <input
          className="input-log w-full"
          type="text"
          name="text"
          onChange={(e) => setUserSearch(e.target.value)}
          value={userSearch}
          placeholder="Rechercher"
        />
      </div>
      {userSearch ? (
        <ul className="w-4/12">
          {usersData.map((user) => {
            if (
              user.pseudo.toLowerCase().includes(userSearch.toLowerCase()) &&
              user._id !== userData._id
            ) {
              return (
                <li
                  className="flex justify-between items-center m-2 border-2 border-cyan-300 rounded-lg min-w-full p-2"
                  key={user._id}
                >
                  <NavLink to={`/profil/${user.pseudo}`} exact>
                    <div className="flex justify-center items-center">
                      <img
                        className="h-12 w-12 rounded-[46px] object-cover mr-3 mb-7.5 bg-white"
                        src={user.picture}
                        alt="user-pic"
                      />
                      <h4 className="ml-2">{user.pseudo}</h4>
                    </div>
                  </NavLink>
                  <div className="">
                    <FollowHandler idToFollow={user._id} type={"suggestion"} />
                  </div>
                </li>
              );
            }
            return null;
          })}
        </ul>
      ) : null}
    </div>
  );
};

export default Search;
