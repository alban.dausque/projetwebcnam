import { combineReducers } from "redux";
import userReducer from "./user.reducer";
import usersReducer from "./users.reducer";
import postReducer from "./post.reducers";
import errorReducer from "./error.reducer";
import competitionsReducer from "./competition.reducer";

export default combineReducers({
    userReducer,
    usersReducer,
    postReducer,
    errorReducer,
    competitionsReducer
})