import {
  DELETE_COMPETITION,
  DELETE_PARTICIPANT,
  GET_COMPETITIONS,
  UPDATE_PARTICIPANT,
} from "../actions/competition.actions";

const initialState = {};

export default function competitionsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMPETITIONS:
      return action.payload;
    case DELETE_COMPETITION:
      return state.filter(
        (competition) => competition._id !== action.payload.competitionId
      );
    case UPDATE_PARTICIPANT:
      return state.map((competition) => {
        if (competition._id === action.payload.competitionId) {
          return {
            ...competition,
            participants: competition.participants.map((participant) => {
              if (participant._id === action.payload.participantId) {
                return {
                  ...participant,
                  rank: action.payload.rank,
                };
              } else {
                return participant;
              }
            }),
          };
        } else return competition;
      });
    case DELETE_PARTICIPANT:
      return state.map((competition) => {
        if (competition._id === action.payload.competitionId) {
          return {
            ...competition,
            participants: competition.participants.filter(
              (participant) => participant._id !== action.payload.participantId
            ),
          };
        } else return competition;
      });
    default:
      return state;
  }
}
