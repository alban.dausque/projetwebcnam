import React, { useState } from "react";
import SignUpForm from "./SignUpForm";
import SignInForm from "./SignInForm";

const Log = ( props ) => {
    const [signUpModal, setSignUpModal] = useState(props.signup);
    const [signInModal, setSignInModal] = useState(props.signin);

    const handleModals = (e) => {
        if (e.target.id === "register") {
            setSignInModal(false);
            setSignUpModal(true);
        } else if (e.target.id === "login") {
            setSignInModal(true);
            setSignUpModal(false);
        }
    }

  return (
    <div className="cadre-log w-4/12">
      <div className="flex items-stretch m-6">
        <ul className="flex flex-col items-center w-2/5">
          <li className={signUpModal ? "active-btn" : "m-2 text-center"} onClick={handleModals} id="register" >S'inscrire</li>
          <li className={signInModal ? "active-btn" : "m-2 text-center"} onClick={handleModals} id="login">Se connecter</li>
        </ul>
        {signUpModal && <SignUpForm />}
        {signInModal && <SignInForm />}
      </div>
    </div>
  );
};

export default Log;
