import React from "react";
import { useDispatch } from "react-redux";
import { deletePost } from "../../actions/post.actions";

const DeleteCard = (props) => {
  const dispatch = useDispatch();

  const deleteQuote = () => dispatch(deletePost(props.id));

  return (
    <div
      className="bg-cyan-100 rounded-full p-1 m-1"
      onClick={() => {
        if (window.confirm("Voulez-vous supprimer ce post ?")) {
          deleteQuote();
        }
      }}
    >
      <img className="h-8 w-8" src="/img/icons/trash.svg" alt="trash" />
    </div>
  );
};

export default DeleteCard;
