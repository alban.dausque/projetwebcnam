import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { dateParser, isEmpty } from "../Utils";
import FollowHandler from "../Profil/FollowHandler";
import { updatePost } from "../../actions/post.actions";
import LikeButton from "./LikeButton";
import DeleteCard from "./DeleteCard";
import CardComments from "./CardComments";
import { NavLink } from "react-router-dom";

const Card = ({ post }) => {
  const [isUpdated, setIsUpdated] = useState(false);
  const [textUpdate, setTextUpdate] = useState(null);
  const [showComments, setShowComments] = useState(false);
  const usersData = useSelector((state) => state.usersReducer);
  const userData = useSelector((state) => state.userReducer);
  const dispatch = useDispatch();

  const updateItem = () => {
    if (textUpdate) {
      dispatch(updatePost(post._id, textUpdate));
    }
    setIsUpdated(false);
  };

  return (
    <li
      className="grid grid-cols-[66px_1fr] border-2 border-cyan-300 m-2 mx-auto p-4 rounded-lg relative min-h-24"
      key={post._id}
    >
      <div className="">
        <img
          className="h-14 w-14 rounded-md shadow-sm object-cover"
          src={
            !isEmpty(usersData[0]) &&
            usersData
              .map((user) => {
                if (user._id === post.posterId) return user.picture;
                else return null;
              })
              .join("")
          }
          alt="poster-pic"
        />
      </div>
      <div className="">
        <div className="flex justify-between my-px min-h-9">
          <div className="flex">
            <NavLink
              to={`/profil/${
                !isEmpty(usersData[0]) &&
                usersData
                  .map((user) => {
                    if (user._id === post.posterId) return user.pseudo;
                    else return null;
                  })
                  .join("")
              }`}
              exact
            >
              <h3 className="text-lg">
                {!isEmpty(usersData[0]) &&
                  usersData
                    .map((user) => {
                      if (user._id === post.posterId) return user.pseudo;
                      else return null;
                    })
                    .join("")}
              </h3>
            </NavLink>
            {post.posterId !== userData._id && (
              <FollowHandler idToFollow={post.posterId} type={"card"} />
            )}
          </div>
          <span>{dateParser(post.createdAt)}</span>
        </div>
        {isUpdated === false && (
          <p className="py-2 mb-2 text-lg whitespace-pre-wrap">
            {post.message}
          </p>
        )}
        {isUpdated && (
          <div className="">
            <textarea
              className="w-full border border-cyan-600 bg-white rounded-2xl px-4 py-2 mb-2 text-lg"
              defaultValue={post.message}
              onChange={(e) => setTextUpdate(e.target.value)}
            />
            <div className="">
              <button className="active-btn" onClick={updateItem}>
                Valider modification
              </button>
            </div>
          </div>
        )}
        {post.picture && post.picture !== "/uploads/posts/undefined" && (
          <img
            src={post.picture}
            alt="card-pic"
            className="w-full rounded-md mt-3 shadow"
          />
        )}
        {userData._id === post.posterId && (
          <div className="flex justify-end">
            <div
              className="bg-cyan-100 rounded-full p-1 m-1"
              onClick={() => setIsUpdated(!isUpdated)}
            >
              <img className="h-8 w-8" src="/img/icons/edit.svg" alt="edit" />
            </div>
            <DeleteCard id={post._id} />
          </div>
        )}
        <div className="flex items-center justify-evenly py-3 px-0">
          <div className="flex items-center">
            <img
              onClick={() => setShowComments(!showComments)}
              src="/img/icons/message1.svg"
              alt="comment"
            />
            <span>{post.comments.length}</span>
          </div>
          <LikeButton post={post} />
        </div>
        {showComments && <CardComments post={post} />}
      </div>
    </li>
  );
};

export default Card;
