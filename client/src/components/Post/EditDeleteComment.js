import React, { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { deleteComment, editComment } from "../../actions/post.actions";
import { UidContext } from "../AppContext";

const EditDeleteComment = ({ comment, postId }) => {
  const [isAuthor, setIsAuthor] = useState(false);
  const [edit, setEdit] = useState(false);
  const [text, setText] = useState("");
  const uid = useContext(UidContext);
  const dispatch = useDispatch();

  const handleEdit = (e) => {
    e.preventDefault();

    if (text) {
      dispatch(editComment(postId, comment._id, text));
      setText("");
      setEdit(false);
    }
  };

  const handleDelete = () => dispatch(deleteComment(postId, comment._id));

  useEffect(() => {
    const checkAuthor = () => {
      if (uid === comment.commenterId) {
        setIsAuthor(true);
      }
    };
    checkAuthor();
  }, [uid, comment.commenterId]);

  return (
    <div className="flex justify-end">
      {isAuthor && edit === false && (
        <span
          className="bg-cyan-100 rounded-full p-1"
          onClick={() => setEdit(!edit)}
        >
          <img
            className="h-8 w-8"
            src="/img/icons/edit.svg"
            alt="edit-comment"
          />
        </span>
      )}
      {isAuthor && edit && (
        <form action="" onSubmit={handleEdit} className="w-full mt-2">
          <input
            className="input-log w-full"
            type="text"
            name="text"
            onChange={(e) => setText(e.target.value)}
            defaultValue={comment.text}
          />
          <br />
          <div className="flex justify-end">
            <label
              className="bg-white rounded-xl m-2 px-4 py-1 text-center"
              onClick={() => setEdit(!edit)}
            >
              Annuler
            </label>
            <span
              className="bg-cyan-100 rounded-full p-1 m-1"
              onClick={() => {
                if (window.confirm("Voulez-vous supprimer ce commentaire ?")) {
                  handleDelete();
                }
              }}
            >
              <img
                className="h-8 w-8"
                src="/img/icons/trash.svg"
                alt="delete"
              />
            </span>
            <input
              className="active-btn"
              type="submit"
              value="Valider modification"
            />
          </div>
        </form>
      )}
    </div>
  );
};

export default EditDeleteComment;
