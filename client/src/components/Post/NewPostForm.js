import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty, timestampParser } from "../Utils";
import { addPost, getPosts } from "../../actions/post.actions";

const NewPostForm = () => {
  const [message, setMessage] = useState("");
  const [postPicture, setPostPicture] = useState(null);
  const [file, setFile] = useState();
  const userData = useSelector((state) => state.userReducer);
  const error = useSelector((state) => state.errorReducer.postError);
  const dispatch = useDispatch();

  const handlePost = async () => {
    if (message || postPicture) {
      const data = new FormData();
      data.append("posterId", userData._id);
      data.append("message", message);
      if (file) data.append("file", file);

      await dispatch(addPost(data));
      dispatch(getPosts());
      cancelPost();
    } else {
      alert("Veuillez entrer un message");
    }
  };

  const handlePicture = (e) => {
    setPostPicture(URL.createObjectURL(e.target.files[0]));
    setFile(e.target.files[0]);
  };

  const cancelPost = () => {
    setMessage("");
    setPostPicture("");
    setFile("");
  };

  return (
    <div className="bg-cyan-200 m-3 w-2/5 mx-auto p-4 rounded-lg relative">
      <div className="flex flex-col items-center text-center px-4">
        <textarea
          className="min-h-20 w-full border rounded-2xl"
          name="message"
          id="message"
          placeholder="Quoi de neuf ?"
          onChange={(e) => setMessage(e.target.value)}
          value={message}
        />
        {message || postPicture ? (
          <li className="grid grid-cols-[66px_1fr] border-2 border-cyan-300 bg-white m-2 mx-auto p-4 rounded-lg relative min-h-24 w-full">
            <div className="">
              <img
                className="h-14 w-14 rounded-md shadow-sm object-cover"
                src={userData.picture}
                alt="user-pic"
              />
            </div>
            <div className="">
              <div className="flex justify-between my-px min-h-9">
                <div className="flex">
                  <h3 className="text-lg">{userData.pseudo}</h3>
                </div>
                <span>{timestampParser(Date.now())}</span>
              </div>
              <div className="text-left">
                <p className="py-2 mb-2 text-lg whitespace-pre-wrap">
                  {message}
                </p>
                <img
                  className="w-full rounded-md mt-3 shadow"
                  src={postPicture}
                  alt=""
                />
              </div>
            </div>
          </li>
        ) : null}
        <div className="flex justify-between w-full">
          <div className="flex">
            <label htmlFor="file-upload">
              <img
                src="/img/icons/picture.svg"
                alt="img"
                style={{ cursor: "pointer" }}
              />
            </label>
            <input
              type="file"
              id="file-upload"
              name="file"
              accept=".jpg, .jpeg, .png"
              onChange={(e) => handlePicture(e)}
              className="hidden"
            />
            <div>
              {!isEmpty(error.format) && (
                <p className="error-log">{error.format}</p>
              )}
              {!isEmpty(error.maxSize) && (
                <p className="error-log">{error.maxSize}</p>
              )}
            </div>
          </div>
          <div className="btn-send">
            {message || postPicture ? (
              <button
                className="bg-white rounded-xl m-2 px-4 py-1 text-center"
                onClick={cancelPost}
              >
                Annuler message
              </button>
            ) : null}
            <button className="active-btn" onClick={handlePost}>
              Envoyer
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewPostForm;
