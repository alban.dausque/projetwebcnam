import React from "react";
import { NavLink } from "react-router-dom";

const LateralNav = () => {
  return (
    <div className="fixed right-3">
      <NavLink to="/" exact>
        <img className="h-11 w-11" src="/img/icons/home.svg" alt="home" />
      </NavLink>
      <br />
      <NavLink to="/search" exact>
        <img className="h-11 w-11" src="/img/icons/search.svg" alt="search" />
      </NavLink>
      <br />
      <NavLink to="/profil" exact>
        <img className="h-11 w-11" src="/img/icons/user.svg" alt="profil" />
      </NavLink>
      <br />
      <NavLink to="/competition" exact>
        <img className="h-11 w-11" src="/img/icons/medal.svg" alt="competition" />
      </NavLink>
    </div>
  );
};

export default LateralNav;
