import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import UploadImg from "./UploadImg";
import { updateBio } from "../../actions/user.actions";
import FollowHandler from "./FollowHandler";

const UpdateProfil = () => {
  const [bio, setBio] = useState("");
  const [updateForm, setUpdateForm] = useState(false);
  const userData = useSelector((state) => state.userReducer);
  const usersData = useSelector((state) => state.usersReducer);
  const error = useSelector((state) => state.errorReducer.userError);
  const dispatch = useDispatch();
  const [followingPopup, setFollowingPopup] = useState(false);
  const [followersPopup, setFollowersPopup] = useState(false);

  const handleUpdate = () => {
    dispatch(updateBio(userData._id, bio));
    setUpdateForm(false);
  };

  return (
    <div className="flex flex-col items-center">
      <h1 className="text-3xl"> Profil de {userData.pseudo}</h1>
      <div className="grid grid-cols-2">
        <div className="cadre-log flex flex-col justify-center items-center min-w-96">
          <h3>Photo de profil</h3>
          <img
            className="h-56 w-56 rounded-[46px] object-cover mb-7.5 bg-white"
            src={userData.picture}
            alt="user-pic"
          />
          <UploadImg />
          <p className="error-log">{error.maxSize}</p>
          <p className="error-log">{error.format}</p>
        </div>
        <div className="cadre-log flex flex-col justify-center items-center">
          <div className="flex flex-col justify-center items-center w-full px-5">
            <h3>Bio</h3>
            {updateForm === false && (
              <>
                <p className="min-h-20 w-full border border-cyan-600 rounded-2xl px-4 py-2 mb-2 text-lg whitespace-pre-wrap">
                  {userData.bio}
                </p>
                <button
                  className="active-btn"
                  onClick={() => setUpdateForm(!updateForm)}
                >
                  Modifier bio
                </button>
              </>
            )}
            {updateForm && (
              <>
                <textarea
                  className="min-h-20 w-full border border-cyan-600 bg-white rounded-2xl px-4 py-2 mb-2 text-lg"
                  type="text"
                  defaultValue={userData.bio}
                  onChange={(e) => setBio(e.target.value)}
                ></textarea>
                <button className="active-btn" onClick={handleUpdate}>
                  Valider modifications
                </button>
              </>
            )}
          </div>
          <br />
          <h5 className="active-btn" onClick={() => setFollowingPopup(true)}>
            Abonnements : {userData.following ? userData.following.length : ""}
          </h5>
          <h5 className="active-btn" onClick={() => setFollowersPopup(true)}>
            Abonnés : {userData.followers ? userData.followers.length : ""}
          </h5>
        </div>
      </div>
      {followingPopup && (
        <div className="fixed top-0 left-0 z-50 w-full h-full flex justify-center">
          <div className="back-blur" />
          <div className="cadre-log absolute top-10 min-w-48">
            <div className="flex">
              <h3>Abonnements</h3>
              <span
                className="absolute right-6 cursor-pointer"
                onClick={() => setFollowingPopup(false)}
              >
                &#10005;
              </span>
            </div>

            <ul>
              {usersData.map((user) => {
                for (let i = 0; i < userData.following.length; i++) {
                  if (user._id === userData.following[i]) {
                    return (
                      <li
                        className="flex justify-between items-center m-2"
                        key={user._id}
                      >
                        <div className="flex items-center">
                          <img
                            className="h-12 w-12 rounded-[46px] object-cover mr-3 mb-7.5 bg-white"
                            src={user.picture}
                            alt="user-pic"
                          />
                          <h4 className="mr-6">{user.pseudo}</h4>
                        </div>
                        <FollowHandler
                          idToFollow={user._id}
                          type={"suggestion"}
                        />
                      </li>
                    );
                  }
                }
                return null;
              })}
            </ul>
          </div>
        </div>
      )}
      {followersPopup && (
        <div className="fixed top-0 left-0 z-50 w-full h-full flex justify-center">
          <div className="back-blur" />
          <div className="cadre-log absolute top-10 min-w-48">
            <div className="flex">
              <h3>Abonnés</h3>
              <span
                className="absolute right-6 cursor-pointer"
                onClick={() => setFollowersPopup(false)}
              >
                &#10005;
              </span>
            </div>
            <ul>
              {usersData.map((user) => {
                for (let i = 0; i < userData.followers.length; i++) {
                  if (user._id === userData.followers[i]) {
                    return (
                      <li
                        className="flex justify-between items-center m-2"
                        key={user._id}
                      >
                        <div className="flex items-center">
                          <img
                            className="h-12 w-12 rounded-[46px] object-cover mr-3 mb-7.5 bg-white"
                            src={user.picture}
                            alt="user-pic"
                          />
                          <h4 className="mr-6">{user.pseudo}</h4>
                        </div>
                        <FollowHandler
                          idToFollow={user._id}
                          type={"suggestion"}
                        />
                      </li>
                    );
                  }
                }
                return null;
              })}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default UpdateProfil;
