import React, { useState } from "react";
import { useSelector } from "react-redux";
import FollowHandler from "./FollowHandler";

const VisitProfil = ({ user_visit }) => {
  const userData = useSelector((state) => state.userReducer);
  const usersData = useSelector((state) => state.usersReducer);
  const [followingPopup, setFollowingPopup] = useState(false);
  const [followersPopup, setFollowersPopup] = useState(false);

  return (
    <div className="flex flex-col items-center">
      <h1 className="text-3xl"> Profil de {user_visit.pseudo}</h1>
      <div className="grid grid-cols-2">
        <div className="cadre-log flex flex-col justify-center items-center min-w-96">
          <h3>Photo de profil</h3>
          <img
            className="h-56 w-56 rounded-[46px] object-cover mb-7.5 bg-white"
            src={user_visit.picture}
            alt="user-pic"
          />
          <FollowHandler idToFollow={user_visit._id} type={"suggestion"} />
        </div>
        <div className="cadre-log flex flex-col justify-center items-center">
          <div className="flex flex-col justify-center items-center w-full px-5">
            <h3>Bio</h3>
            <p className="min-h-20 w-full border border-cyan-600 rounded-2xl px-4 py-2 mb-2 text-lg whitespace-pre-wrap">
              {user_visit.bio}
            </p>
          </div>
          <br />
          <h5 className="active-btn" onClick={() => setFollowingPopup(true)}>
            Abonnements :{" "}
            {user_visit.following ? user_visit.following.length : ""}
          </h5>
          <h5 className="active-btn" onClick={() => setFollowersPopup(true)}>
            Abonnés : {user_visit.followers ? user_visit.followers.length : ""}
          </h5>
        </div>
      </div>
      {followingPopup && (
        <div className="fixed top-0 left-0 z-50 w-full h-full flex justify-center">
          <div className="back-blur" />
          <div className="cadre-log absolute top-10 min-w-48">
            <div className="flex">
              <h3>Abonnements</h3>
              <span
                className="absolute right-6 cursor-pointer"
                onClick={() => setFollowingPopup(false)}
              >
                &#10005;
              </span>
            </div>

            <ul>
              {usersData.map((user_bis) => {
                for (let i = 0; i < user_visit.following.length; i++) {
                  if (user_bis._id === user_visit.following[i]) {
                    return (
                      <li
                        className="flex justify-between items-center m-2"
                        key={user_bis._id}
                      >
                        <div className="flex items-center">
                          <img
                            className="h-12 w-12 rounded-[46px] object-cover mr-3 mb-7.5 bg-white"
                            src={user_bis.picture}
                            alt="user-pic"
                          />
                          <h4 className="mr-6">{user_bis.pseudo}</h4>
                        </div>
                        {user_bis._id !== userData._id && (
                          <FollowHandler
                            idToFollow={user_bis._id}
                            type={"suggestion"}
                          />
                        )}
                      </li>
                    );
                  }
                }
                return null;
              })}
            </ul>
          </div>
        </div>
      )}
      {followersPopup && (
        <div className="fixed top-0 left-0 z-50 w-full h-full flex justify-center">
          <div className="back-blur" />
          <div className="cadre-log absolute top-10 min-w-48">
            <div className="flex">
              <h3>Abonnés</h3>
              <span
                className="absolute right-6 cursor-pointer"
                onClick={() => setFollowersPopup(false)}
              >
                &#10005;
              </span>
            </div>
            <ul>
              {usersData.map((user_bis) => {
                for (let i = 0; i < user_visit.followers.length; i++) {
                  if (user_bis._id === user_visit.followers[i]) {
                    return (
                      <li
                        className="flex justify-between items-center m-2"
                        key={user_bis._id}
                      >
                        <div className="flex items-center">
                          <img
                            className="h-12 w-12 rounded-[46px] object-cover mr-3 mb-7.5 bg-white"
                            src={user_bis.picture}
                            alt="user-pic"
                          />
                          <h4 className="mr-6">{user_bis.pseudo}</h4>
                        </div>
                        {user_bis._id !== userData._id && (
                          <FollowHandler
                            idToFollow={user_bis._id}
                            type={"suggestion"}
                          />
                        )}
                      </li>
                    );
                  }
                }
                return null;
              })}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default VisitProfil;
