import React from "react";
import { useSelector } from "react-redux";
import { isEmpty } from "../Utils";
import CardCompetition from "./CardCompetition";

const NextCompetition = () => {
  const competitions = useSelector((state) => state.competitionsReducer);
  const today = new Date();

  return (
    <div className="flex flex-col items-center w-full">
      <h3 className="m-3 p-2 border-2 border-cyan-500 rounded-lg">
        Prochaine compétition
      </h3>
      <ul className="w-4/5">
        {!isEmpty(competitions[0]) &&
          competitions.map((competition) => {
            var startDate = new Date(competition.startDate);
            if (startDate > today)
              return (
                <CardCompetition
                  competition={competition}
                  key={competition._id}
                />
              );
            return null;
          })}
      </ul>
    </div>
  );
};

export default NextCompetition;
