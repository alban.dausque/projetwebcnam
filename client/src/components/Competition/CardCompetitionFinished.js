import React, { useContext, useEffect, useState } from "react";
import { UidContext } from "../AppContext";
import { useDispatch } from "react-redux";
import { deleteCompetition } from "../../actions/competition.actions";
import { formatDate, isEmpty } from "../Utils";
import CardParticipant from "./CardParticipant";

const CardCompetitionFinished = ({ competition }) => {
  const [isParticipating, setIsParticipating] = useState(false);
  const uid = useContext(UidContext);
  const dispatch = useDispatch();

  useEffect(() => {
    setIsParticipating(false);
    if (!isEmpty(competition.participants[0]))
      competition.participants.map((participant) => {
        if (participant.participantId === uid) setIsParticipating(true);
        return null;
      });
  }, [competition, uid]);

  const handleDelete = () => dispatch(deleteCompetition(competition._id));

  return (
    <li
      className="border-2 border-cyan-300 m-1 p-4 rounded-lg w-full"
      key={competition._id}
    >
      <div className="flex justify-between">
        <div>{competition.sport}</div>
        <span>{formatDate(competition.startDate)}</span>
      </div>
      <p className="text-left my-1">{competition.description}</p>
      <br />
      <div className="text-left">
        Nombre de participants : {competition.participants.length}
      </div>
      <br />
      <div className="flex justify-between items-center">
        <div></div>
        {isParticipating === true ? (
          <div className="m-1 p-2 border-2 border-cyan-500 rounded-lg">
            Vous avez participé
          </div>
        ) : (
          ""
        )}
        <div className="flex">
          {uid && competition.creatorId === uid && (
            <span
              className="bg-cyan-100 rounded-full p-1 m-1 "
              onClick={() => {
                if (
                  window.confirm("Voulez-vous supprimer cette compétition ?")
                ) {
                  handleDelete();
                }
              }}
            >
              <img
                className="h-8 w-8"
                src="/img/icons/trash.svg"
                alt="delete"
              />
            </span>
          )}
        </div>
      </div>
      <br />
      <ul>
        <li className="grid grid-cols-2">
          <h3>Pseudo</h3>
          <h3>Rang</h3>
        </li>
        {!isEmpty(competition.participants[0]) &&
          competition.participants.map((participant) => {
            return (
              <CardParticipant participant={ participant } competitionId={ competition._id } creatorId={ competition.creatorId } />
            );
          })}
      </ul>
    </li>
  );
};

export default CardCompetitionFinished;
