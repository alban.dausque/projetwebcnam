import axios from "axios";
import React, { useContext, useState } from "react";
import { UidContext } from "../AppContext";
import { getCompetitions } from "../../actions/competition.actions";
import { useDispatch } from "react-redux";
import { useEffect } from "react";

const CreateCompetition = () => {
  const [description, setDescription] = useState("");
  const [startDate, setStartDate] = useState(new Date().getTime());
  const [sport, setSport] = useState("");
  const uid = useContext(UidContext);
  const dispatch = useDispatch();

  const handleCompetition = async () => {
    let creatorId = uid;
    await axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}api/competition/`,
      data: {
        creatorId,
        description,
        startDate,
        sport,
      },
    }).catch((err) => console.log(err));
    dispatch(getCompetitions());
    setDescription('');
    setSport('');
    setDateDefault();
  };

  function setDateDefault() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    setStartDate(today);
  }

  useEffect(() => {
    setDateDefault();
  }, []);

  return (
    <div className="flex flex-col items-center w-full">
      <h3 className="m-3 p-2 border-2 border-cyan-500 rounded-lg">
        Création de compétition
      </h3>
      <div className="flex flex-col w-3/5 bg-cyan-200 m-3 mx-auto p-4 rounded-lg">
        <h3 className="text-left">Sport</h3>
        <textarea
          className="w-full border rounded-2xl my-1"
          name="sport"
          id="sport"
          placeholder="Sport"
          onChange={(e) => setSport(e.target.value)}
          value={sport}
        />
        <br />
        <h3 className="text-left">Date</h3>
        <input
          className="border rounded-2xl my-1"
          type="date"
          id="startDate"
          name="startDate"
          onChange={(e) => setStartDate(e.target.value)}
          value={startDate}
        />
        <br />
        <h3 className="text-left">Description</h3>
        <textarea
          className="min-h-20 w-full border rounded-2xl my-1"
          name="description"
          id="description"
          placeholder="Sport"
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />
        <br />
        <button className="active-btn" onClick={handleCompetition}>
          Envoyer
        </button>
      </div>
    </div>
  );
};

export default CreateCompetition;
