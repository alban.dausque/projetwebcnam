import React, { useContext, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "../Utils";
import { UidContext } from "../AppContext";
import { updateParticipant } from "../../actions/competition.actions";

const CardParticipant = ({ participant, competitionId, creatorId }) => {
  const [rank, setRank] = useState("");
  const usersData = useSelector((state) => state.usersReducer);
  const uid = useContext(UidContext);
  const dispatch = useDispatch();

  const handleUpdateRank = () => {
    dispatch(updateParticipant(competitionId, participant._id, rank));
  };

  return (
    <div>
      <li className="grid grid-cols-2">
        {!isEmpty(usersData[0]) &&
          usersData.map((user) => {
            if (participant.participantId === user._id)
              return (
                <div className="flex justify-center items-center">
                  {user.pseudo}
                </div>
              );
            return null;
          })}
        {uid && creatorId === uid ? (
          <div className="flex justify-center items-center">
            <input
              className="border rounded-2xl my-1 max-w-28 mr-1"
              type="number"
              value={participant.rank}
              onChange={(e) => setRank(e.target.value)}
            />
            <img src="/img/icons/check.svg" alt="check" onClick={handleUpdateRank} />
          </div>
        ) : (
          <div>{participant.rank}</div>
        )}
      </li>
    </div>
  );
};

export default CardParticipant;
