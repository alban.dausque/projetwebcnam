import React from "react";
import { isEmpty } from "../Utils";
import { useSelector } from "react-redux";
import CardCompetitionFinished from "./CardCompetitionFinished";

const ResultCompetition = () => {
  const competitions = useSelector((state) => state.competitionsReducer);
  const today = new Date();

  return (
    <div className="flex flex-col items-center w-full">
      <h3 className="m-3 p-2 border-2 border-cyan-500 rounded-lg">
        Résultat compétition
      </h3>
      <ul className="w-4/5">
        {!isEmpty(competitions[0]) &&
          competitions.map((competition) => {
            var startDate = new Date(competition.startDate);
            if (startDate < today)
              return (
                <CardCompetitionFinished
                  competition={competition}
                  key={competition._id}
                />
              );
            return null;
          })}
      </ul>
    </div>
  );
};

export default ResultCompetition;
