import React, { useContext, useEffect, useState } from "react";
import { UidContext } from "../AppContext";
import { useDispatch } from "react-redux";
import {
  addParticipant,
  deleteCompetition,
  deleteParticipant,
  getCompetitions,
} from "../../actions/competition.actions";
import { formatDate, isEmpty } from "../Utils";

const CardCompetition = ({ competition }) => {
  const [isParticipating, setIsParticipating] = useState(false);
  const uid = useContext(UidContext);
  const dispatch = useDispatch();

  useEffect(() => {
    setIsParticipating(false);
    if (!isEmpty(competition.participants[0]))
      competition.participants.map((participant) => {
        if (participant.participantId === uid) setIsParticipating(true);
        return null;
      });
  }, [competition, uid]);

  const handleAdd = () => {
    if (isParticipating === false) {
      dispatch(addParticipant(competition._id, uid)).then(() =>
        dispatch(getCompetitions())
      );
    } else {
      handleDeleteParticipant();
    }
    setIsParticipating(!isParticipating);
  };

  const handleDeleteParticipant = () => {
    if (!isEmpty(competition.participants[0]))
      competition.participants.map((participant) => {
        if (participant.participantId === uid) console.log("trouvé");
        dispatch(deleteParticipant(competition._id, participant._id));
        return null;
      });
  };

  const handleDelete = () => dispatch(deleteCompetition(competition._id));

  return (
    <li
      className="border-2 border-cyan-300 m-1 p-4 rounded-lg w-full"
      key={competition._id}
    >
      <div className="flex justify-between">
        <div>{competition.sport}</div>
        <span>{formatDate(competition.startDate)}</span>
      </div>
      <p className="text-left my-1">{competition.description}</p>
      <br />
      <div className="text-left">
        Nombre de participants : {competition.participants.length}
      </div>
      <br />
      <div className="flex justify-between items-center">
        <div></div>
        {isParticipating === true ? (
        <button className="active-btn" onClick={handleAdd}>
          Vous participez
        </button>
        ) : (
        <button className="active-btn" onClick={handleAdd}>
          Participer
        </button>
        )}
        <div className="flex">
          {uid && competition.creatorId === uid && (
            <span
              className="bg-cyan-100 rounded-full p-1 m-1 "
              onClick={() => {
                if (
                  window.confirm("Voulez-vous supprimer cette compétition ?")
                ) {
                  handleDelete();
                }
              }}
            >
              <img
                className="h-8 w-8"
                src="/img/icons/trash.svg"
                alt="delete"
              />
            </span>
          )}
        </div>
      </div>
    </li>
  );
};

export default CardCompetition;
