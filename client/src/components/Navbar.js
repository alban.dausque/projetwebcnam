import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { UidContext } from "./AppContext";
import Logout from "./Log/Logout";

const Navbar = () => {
  const uid = useContext(UidContext);
  const userData = useSelector((state) => state.userReducer);

  return (
    <nav>
      <div className="nav-container lg:flex lg:items-center lg:justify-between">
        <div className="logo">
          <NavLink to="/">
            <div className="logo">
              <img className="h-20 w-20" src="/img/icon.png" alt="icon" />
              <h3>Sports Social Network</h3>
            </div>
          </NavLink>
        </div>
        {uid ? (
          <ul className="flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap sm:space-x-6 items-center">
            <li></li>
            <li>
              <NavLink to="/profil">
                <h5>Bienvenue {userData.pseudo}</h5>
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/profil">
                <img
                  className="h-14 w-14 rounded-md shadow-sm object-cover"
                  src={userData.picture}
                  alt="user-img"
                />
              </NavLink>
            </li>
            <Logout />
          </ul>
        ) : (
          <ul>
            <li></li>
            <li>
              <NavLink to="/profil">
                <div className="nav-btn-log">Se connecter</div>
              </NavLink>
            </li>
          </ul>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
