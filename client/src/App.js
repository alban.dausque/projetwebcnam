import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import Home from "./pages/Home.js";
import Profil from "./pages/Profil.js";
import Navbar from "./components/Navbar.js";
import { UidContext } from "./components/AppContext.js";
import axios from "axios";
import { useDispatch } from "react-redux";
import { getUser } from "./actions/user.actions.js";
import Search from "./pages/Search.js";
import Competition from "./pages/Competition.js";

const App = () => {
  const [uid, setUid] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchToken = async () => {
      await axios({
        method: "get",
        url: `${process.env.REACT_APP_API_URL}jwtid`,
        withCredentials: true,
      })
        .then((res) => {
          setUid(res.data);
        })
        .catch((err) => console.log("No token"));
    };
    fetchToken();

    if (uid) dispatch(getUser(uid));
  }, [uid, dispatch]);

  return (
    <UidContext.Provider value={uid}>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/profil" element={<Profil />} />
          <Route path="/profil/:pseudo" element={<Profil />} />
          <Route path="/search" element={<Search />} />
          <Route path="/competition" element={<Competition />} />
          <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
      </Router>
    </UidContext.Provider>
  );
};

export default App;
