const router = require("express").Router();
const postController = require("../controllers/post.controller");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./client/public/uploads/posts"); // dossier de stockage des images de posts
  },
  filename: function (req, file, cb) {
    const fileName = Date.now() + ".jpg";
    cb(null, fileName);
  },
});
const upload = multer({ storage: storage });

// post
router.get("/", postController.readPost);
router.post("/", upload.single("file"), postController.createPost);
router.put("/:id", postController.updatePost);
router.delete("/:id", postController.deletePost);
router.patch("/like/:id", postController.likePost);
router.patch("/unlike/:id", postController.unlikePost);

// commentaire
router.patch("/comment-post/:id", postController.commentPost);
router.patch("/edit-comment-post/:id", postController.editCommentPost);
router.patch("/delete-comment-post/:id", postController.deleteCommentPost);

module.exports = router;
