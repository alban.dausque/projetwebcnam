const router = require("express").Router();
const competitionController = require("../controllers/competition.controller");

// Compétition
router.get("/", competitionController.readCompetition);
router.post("/", competitionController.createCompetition);
router.put("/:id", competitionController.updateCompetition);
router.delete("/:id", competitionController.deleteCompetition);

// Participants
router.patch("/participants/:id", competitionController.addParticipant);
router.patch("/update-participant/:id", competitionController.updateParticipants);
router.patch("/delete-participants/:id", competitionController.deleteParticipant);

module.exports = router;